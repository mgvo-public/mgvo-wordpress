<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * 
 * @since             0.1.0
 * @package           Mgvo_Wordpress
 *
 * @wordpress-plugin
 * Plugin Name:       mgvo-wordpress
 * Plugin URI:        mgvo-wordpress
 * Description:       Imports information from MGVO
 * Version:           1.0.0
 * Author:            Michael Kours
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       mgvo-wordpress
 * Domain Path:       /languages
 GitHub Plugin URI: https://gitlab.com/michaelbihn/mgvo_api
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'PLUGIN_MGVO_WORDPRESS_VERSION', '0.1.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-mgvo-wordpress-activator.php
 */
function activate_mgvo_wordpress() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-mgvo-wordpress-activator.php';
	Mgvo_Wordpress_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-mgvo-wordpress-deactivator.php
 */
function deactivate_mgvo_wordpress() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-mgvo-wordpress-deactivator.php';
	Mgvo_Wordpress_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_mgvo_wordpress' );
register_deactivation_hook( __FILE__, 'deactivate_mgvo_wordpress' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-mgvo-wordpress.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_mgvo_wordpress() {

	$plugin = new Mgvo_Wordpress();
	$plugin->run();

}
run_mgvo_wordpress();
