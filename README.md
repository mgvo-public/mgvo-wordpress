
## MGVO API
Dieses Repository stellt den Code für einen externen Zugriff auf MGVO über eine API bereit.
In der Datei /libs/mgvo-hpapi.php ist die eigentliche API. In der Datei /libs/mgvo-sniplets.php befinden sich Funktionen, die auf der API aufbauen und fertige HTML-Sniplets zur Integration in eine Homepage anbieten.

## MGVO Wordpress Plugin
Hierauf aufbauend gibt es auch ein fertiges Wordpress-Plugin (https://gitlab.com/mgvo-public/mgvo-wordpress), welches die Sniplets entsprechend einbindet.
Durch Anpassung nur in den Sniplets kann ohne tiefes Wordpress Wissen sehr leicht eine individuelle Wordpress-Integration vorgenommen werden. 

## Installation
Entweder kann dieses Repository in das Wordpress-Plugin Ordner gecloned werden. Dann muss das Repository der API separat in das Verzeichnis mgvo-api gecloned werden.
Oder es wird das fertige Zip von der MGVO-Hilfeseite verwendet, dieses enthält auch die API. Dieses kann über ihre Wordpress-Seite als Plugin direkt hochgeladen und installiert werden.

## Sicherheit
Es werden teilweise Funktionen bereit gestellt, die eine höhere Anforderung an die Datensicherheit haben. Zur Nutzung dieser Methoden muss im System ein geheimer Schlüssel aus "System -> Basiseinstellungen -> Allg. Parameter", Technik hinterlegt werden. Dieser Schlüssel wird bei der Instanziierung der API-Klasse als Parameter mitgegeben. Im Wordpress-Plugin müssen diese Angaben auf den Einstellungsseiten hinterlegt werden. 



