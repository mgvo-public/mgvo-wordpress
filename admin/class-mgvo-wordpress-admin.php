<?php

/**
 * The admin-specific functionality of the plugin.
 *
  * @since      1.0.0
 *
 * @package    Mgvo_Wordpress
 * @subpackage Mgvo_Wordpress/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Mgvo_Wordpress
 * @subpackage Mgvo_Wordpress/admin
 * @author     Michael Kours
 */
class Mgvo_Wordpress_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->set_options();
	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Mgvo_Wordpress_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Mgvo_Wordpress_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/mgvo-wordpress-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Mgvo_Wordpress_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Mgvo_Wordpress_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/mgvo-wordpress-admin.js', array( 'jquery' ), $this->version, false );

	}

	/**
	 * Adds a settings page link to a menu
	 *
	 * @link 		https://codex.wordpress.org/Administration_Menus
	 * @since 		1.0.0
	 * @return 		void
	 */
	public function add_menu() {

		// Top-level page
		// add_menu_page( $page_title, $menu_title, $capability, $menu_slug, $function, $icon_url, $position );

		// Submenu Page
		// add_submenu_page( $parent_slug, $page_title, $menu_title, $capability, $menu_slug, $function);

		add_options_page(
			apply_filters( $this->plugin_name . '-settings-page-title', esc_html__( 'MGVO Settings', 'mgvo-wordpress-settings' ) ),
			apply_filters( $this->plugin_name . '-settings-menu-title', esc_html__( 'MGVO', 'mgvo-wordpress-settings' ) ),
			'manage_options',
			$this->plugin_name . '-settings',
			array( $this, 'page_options' )
		);


	} // add_menu()

	/**
	 * Creates the options page
	 *
	 * @since 		1.0.0
	 * @return 		void
	 */
	public function page_options() {

		include( plugin_dir_path( __FILE__ ) . 'partials/mgvo-wordpress-admin-page-settings.php' );

	} // page_options()

	/**
	 * Registers settings sections with WordPress
	 */
	public function register_sections() {

		// add_settings_section( $id, $title, $callback, $menu_slug );

		add_settings_section(
			$this->plugin_name . '-basic',
			apply_filters( $this->plugin_name . 'section-basic', esc_html__( 'Basic', 'mgvo' ) ),
			array( $this, 'section_basic' ),
			$this->plugin_name
		);

	} // register_sections()

		/**
	 * Creates a settings section
	 *
	 * @since 		1.0.0
	 * @param 		array 		$params 		Array of parameters for the section
	 * @return 		mixed 						The settings section
	 */
	public function section_basic( $params ) {

		include( plugin_dir_path( __FILE__ ) . 'partials/mgvo-wordpress-admin-section-basic.php' );

	} // section_basic()

		/**
	 * Registers plugin settings
	 *
	 * @since 		1.0.0
	 * @return 		void
	 */
	public function register_settings() {

		// register_setting( $option_group, $option_name, $sanitize_callback );

		register_setting(
			$this->plugin_name . '-options',
			$this->plugin_name . '-options',
			array( $this, 'validate_options' )
		);

	} // register_settings()

/**
	 * Registers settings fields with WordPress
	 */
	public function register_fields() {

		// add_settings_field( $id, $title, $callback, $menu_slug, $section, $args );
		add_settings_field(
			'mgvo_call_id',
			apply_filters( $this->plugin_name . 'label-mgvo-call-id', esc_html__( 'Call ID', 'mgvo' ) ),
			array( $this, 'field_text' ),
			$this->plugin_name,
			$this->plugin_name . '-basic',
			array(
				'description' 	=> 'Unique call ID to connect to MGVO',
				'id' 			=> 'mgvo_call_id'
			)
		);

		add_settings_field(
			'mgvo_secret',
			apply_filters( $this->plugin_name . 'label-mgvo-secret', esc_html__( 'Secret key', 'mgvo' ) ),
			array( $this, 'field_text' ),
			$this->plugin_name,
			$this->plugin_name . '-basic',
			array(
				'description' 	=> 'Secret key to connect to MGVO',
				'id' 			=> 'mgvo_secret'
			)
		);
		
		add_settings_field(
			'mgvo_chache_time',
			apply_filters( $this->plugin_name . 'label-mgvo-chache-time', esc_html__( 'Cache Time in min', 'mgvo' ) ),
			array( $this, 'field_text' ),
			$this->plugin_name,
			$this->plugin_name . '-basic',
			array(
				'description' 	=> 'Cache Time in Min.',
				'id' 			=> 'mgvo_chache_time'
			)
	   );
		   
		   add_settings_field(
		      'mgvo_mitantrag_load_bootstrap',
		      apply_filters( $this->plugin_name . 'label-mgvo_mitantrag_load_bootstrap', esc_html__( 'Load Bootstrap Files for Mitgliedsantrag', 'mgvo' ) ),
		      array( $this, 'field_text' ),
		      $this->plugin_name,
		      $this->plugin_name . '-basic',
		      array(
		         'description' 	=> 'Version (5.02, 5.2 or 5.3)',
		         'id' 			=> 'mgvo_mitantrag_load_bootstrap'
		      )
		      );
			
		/* add_settings_field(
			'mgvo_login_in_wp',
			apply_filters( $this->plugin_name . 'label-mgvo-login-in-wp', esc_html__( 'Aktivate user WP-Validation over MGVO', 'mgvo' ) ),
			array( $this, 'field_text' ),
			$this->plugin_name,
			$this->plugin_name . '-login',
			array(
				'description' 	=> 'WP-Login über MGVO',
				'id' 			=> 'mgvo_login_in_wp'
			)	 );
		*/
		/* add_settings_field(
			'mgvo_kalender_number',
			apply_filters( $this->plugin_name . 'label-kalender-number', esc_html__( 'Kalender number', 'mgvo' ) ),
			array( $this, 'field_text' ),
			$this->plugin_name,
			$this->plugin_name . '-basic',
			array(
				'description' 	=> 'Kalender Number, which will be used',
				'id' 			=> 'mgvo_kalender_number'
			) );
		*/
		
		
		add_settings_field(
		   'mgvo_mitantrag_form',
		   apply_filters( $this->plugin_name . 'label-mgvo-mitantrag-form', esc_html__( 'Mitgliedsantrag Formularname', 'mgvo' ) ),
		   array( $this, 'field_text' ),
		   $this->plugin_name,
		   $this->plugin_name . '-basic',
		   array(
		      'description' 	=> 'Mitgliedsantrag Formularname ohne Pfad (Pfad ist immer plugins/mgvo-wordpress/mgvo-api/libs/mitantrag"), für Default-Formular leer lassen',
		      'id' 			=> 'mgvo_mitantrag_form'
		   )
		);
		

		add_settings_field(
			'mgvo_mitantrag_settings',
			apply_filters( $this->plugin_name . 'label-mgvo-mitantrag-settings', esc_html__( 'Mitantrag Settings', 'mgvo' ) ),
			array($this, 'field_textarea'),
			$this->plugin_name,
			$this->plugin_name . '-basic',
			array(
				'description' 	=> 'Mitantrag Settings',
				'id' 			=> 'mgvo_mitantrag_settings'
			)
		);
		
	} // register_fields()

	public function field_textarea(){
		$value = $this->options['mgvo_mitantrag_settings'] ?? "";
		/**
		 * Wenn Settings noch nicht gesetzt, dann default aus JSON laden
		 */
		if(empty($value)){
			$jsonFile = plugin_dir_path( dirname(__FILE__) ).'mgvo-api/libs/mitantrag/mitantragSettings.json';
			$jsonString = file_get_contents($jsonFile);
			$value = $jsonString;
		}
		echo '<textarea id="mgvo_mitantrag_settings" name='.$this->plugin_name."-options[mgvo_mitantrag_settings]".' rows="80" cols="100">';
		echo json_encode(json_decode($value), JSON_PRETTY_PRINT);
		echo "</textarea>";
	}


		/**
	 * Returns an array of options names, fields types, and default values
	 *
	 * @return 		array 			An array of options
	 */
	public static function get_options_list() {

		$options = array();

		$options[] = array( 'mgvo_call_id', 'text', '' );
		$options[] = array( 'mgvo_secret', 'text', '' );
		$options[] = array( 'mgvo_mitantrag_form', 'text', '' );
		/*$options[] = array( 'mgvo_kalender_number', 'text', '' );*/
		$options[] = array( 'mgvo_chache_time', 'text', '' );
		$options[] = array( 'mgvo_mitantrag_settings', 'text', '' );
		return $options;
	} // get_options_list()



		/**
	 * Sets the class variable $options
	 */
	private function set_options() {

		$this->options = get_option( $this->plugin_name . '-options' );

	} // set_options()

	/**
	 * Validates saved options
	 *
	 * @since 		1.0.0
	 * @param 		array 		$input 			array of submitted plugin options
	 * @return 		array 						array of validated plugin options
	 */
	public function validate_options( $input ) {

		//wp_die( print_r( $input ) );

		$valid 		= array();
		$options 	= $this->get_options_list();

		foreach ( $options as $option ) {

			$name = $option[0];
			$type = $option[1];

			$valid[$option[0]] = $this->sanitizer( $type, $input[$name] );

		}

		return $valid;

	} // validate_options()

	private function sanitizer( $type, $data ) {

		if ( empty( $type ) ) { return; }
		if ( empty( $data ) ) { return; }

		$return 	= '';
		$sanitizer 	= new MGVO_Wordpress_Sanitize();

		$sanitizer->set_data( $data );
		$sanitizer->set_type( $type );

		$return = $sanitizer->clean();

		unset( $sanitizer );

		return $return;

	} // sanitizer()

		/**
	 * Creates a text field
	 *
	 * @param 	array 		$args 			The arguments for the field
	 * @return 	string 						The HTML field
	 */
	public function field_text( $args ) {

		$defaults['class'] 			= 'text widefat';
		$defaults['description'] 	= '';
		$defaults['label'] 			= '';
		$defaults['name'] 			= $this->plugin_name . '-options[' . $args['id'] . ']';
		$defaults['placeholder'] 	= '';
		$defaults['type'] 			= 'text';
		$defaults['value'] 			= '';

		apply_filters( $this->plugin_name . '-field-text-options-defaults', $defaults );

		$atts = wp_parse_args( $args, $defaults );

		if ( ! empty( $this->options[$atts['id']] ) ) {

			$atts['value'] = $this->options[$atts['id']];

		}

		include( plugin_dir_path( __FILE__ ) . 'partials/' . $this->plugin_name . '-admin-field-text.php' );

	} // field_text()



}
