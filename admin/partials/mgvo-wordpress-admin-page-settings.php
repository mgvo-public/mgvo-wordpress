<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @since      1.0.0
 *
 * @package    Mgvo_Wordpress
 * @subpackage Mgvo_Wordpress/admin/partials
 */
?>

<h2><?php echo esc_html( get_admin_page_title() ); ?></h2>
<form method="post" action="options.php"><?php

settings_fields( $this->plugin_name . '-options' );

do_settings_sections( $this->plugin_name );

?>
Die Call-ID findet sich im MGVO unter System/Homepagelinks. 
Die Homepagelinks müssen vorher in den Einstellungen (MGVO/System/Einstellungsassistent/Technik-
Parameter/ Integration Mitgliederbereich in Vereins-Community aktiviert werden.<br>
<br> 
Das Secret (MGVO/System/Einstellungsassistent/Technikeinstellungen/Geheimer Schlüssel) muss nur eingegeben werden, wenn die Mitgliederliste, eine individuelle Mitgliedsanmeldung oder dem Mitgliedsantrag auf der Webseite benötigt wird. Sonst wird davon abgeraten, diese einzutragen, da mit dieser auf Datenschutzrelevante Informationen zugegriffen werden kann. <br>
<br>
Die Cachedauer bestimmt, wie lange die Daten von MGVO zwischengespeichert werden. Default (kein Eintrag) ist 5min. Es ist hierbei zu beachten, dass MGVO nicht mehr als 10.000 Abrufe von einer IP-Adresse pro Tag zuläßt. Daher sollte der Cache (mit dem Wert = 0) nur in besonderen Fällen (z.B. für Tests) ausgeschaltet werden. 
Alternativ kann bei jedem Short-Code angegeben werden, dass der Cache gelöscht und neu aufgebaut werden soll. D.h. es kann für Entwicklungszwecke auch eine verstecke Seite, die den Cache neu aufbaut erstellt werden. 

<?php


submit_button( 'Save Settings' );

?></form>
