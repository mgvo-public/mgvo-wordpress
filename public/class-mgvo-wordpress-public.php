<?php

/**
 * The public-facing functionality of the plugin.
 *

 * @since      1.0.0 
 *
 * @package    Mgvo_Wordpress
 * @subpackage Mgvo_Wordpress/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Mgvo_Wordpress
 * @subpackage Mgvo_Wordpress/public
 * @author     Michael Kours <webmaster@blau-gold-darmstadt.de>
 */
class Mgvo_Wordpress_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	private $func;
	
	public $call_id;
	
	public $cache;

	public $mitantragSettings;
	
	private $mgvo_secret;
	
	private $mgvo_entrys;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$options = get_option( $this->plugin_name . '-options' );
		
		require_once(plugin_dir_path( dirname( __FILE__ ) ) . "mgvo-api/libs/MgvoHpApi.php");
		require_once(plugin_dir_path( dirname( __FILE__ ) ) . "mgvo-api/libs/MgvoSniplet.php");
		
		if ( ! class_exists("MgvoSnipletVerein",false)) {
		
   		$errHandler = function ($errcode, $errmsg, $fileName, $line) {
   		   return $errcode == E_WARNING
   		   && strpos($errmsg,'include(') !== false
   		   && $fileName == __FILE__ ;
   		};
   		set_error_handler($errHandler);
   		include(plugin_dir_path( dirname( __FILE__ ) ) . "/mgvo-api/libs/MgvoSnipletVerein.php");
   		restore_error_handler();
		}
		
		$this->call_id = $options['mgvo_call_id'] ?? "";
		$this->mgvo_secret = $options['mgvo_secret'] ?? "";
		$this->cache = $options['mgvo_chache_time'] ?? 5;
		$this->mitantragForm =  $options['mgvo_mitantrag_form'] ?? null;
		$this->mitantragSettings = $options['mgvo_mitantrag_settings'] ?? null;
		if ( class_exists("MgvoSnipletVerein",false)) {
		   $this->func = new MgvoSnipletVerein($this->call_id,$this->mgvo_secret, $this->cache);
		} else {
		   $this->func = new MgvoSniplet($this->call_id,$this->mgvo_secret, $this->cache);
		} 
		   
		// Links zu den Einzelseiten beschaffen (wurden im mgvo-wordpress-activator angelegt)
		$this->mgvo_entrys['kalender'] = $this->get_page_by_title("Kalenderdetails");
		$this->mgvo_entrys['gruppen'] = $this->get_page_by_title("Gruppendetails");
		$this->mgvo_entrys['notraining'] = $this->get_page_by_title("Veranstaltungsdetails");
		$this->mgvo_entrys['notraining'] = $this->get_page_by_title("Trainingsausfall Details");
	} 
	
	/**
	 * Since get_page_by_title ist Depricated since Wordpress 6.2 rewirte that funktion to WP_Query()
	 * Source https://gist.github.com/awps/1ec1c4d91e8ce6ddd3810e67b2f9a9d2
	 */
	
	function get_page_by_title( $page_title, $output = OBJECT, $post_type = 'page' ) {
	   $query = new WP_Query(
	      array(
	         'post_type'              => $post_type,
	         'title'                  => $page_title,
	         'post_status'            => 'all',
	         'posts_per_page'         => 1,
	         'no_found_rows'          => true,
	         'ignore_sticky_posts'    => true,
	         'update_post_term_cache' => false,
	         'update_post_meta_cache' => false,
	         'orderby'                => 'date',
	         'order'                  => 'ASC',
	      )
	      );
	   
	   if ( ! empty( $query->post ) ) {
	      $_post = $query->post;
	      
	      if ( ARRAY_A === $output ) {
	         return $_post->to_array();
	      } elseif ( ARRAY_N === $output ) {
	         return array_values( $_post->to_array() );
	      }
	      
	      return $_post;
	   }
	   
	   return null;
	}
	

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Mgvo_Wordpress_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Mgvo_Wordpress_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
	   wp_enqueue_style( "mitantragCSS", plugin_dir_url( __FILE__ )   . "../mgvo-api/libs/mitantrag/mitantrag.css", array(), $this->version, 'all' );
		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/mgvo-wordpress-public.css', array(), $this->version, 'all' );
		
	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Mgvo_Wordpress_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Mgvo_Wordpress_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */ // plugin_dir_path( dirname(__FILE__) 
	   wp_enqueue_script( "mitantrag", plugin_dir_url( __FILE__ ) .'../mgvo-api/libs/mitantrag/mitantrag.js', array( 'jquery' ), $this->version, false );
	   wp_enqueue_script( "mitantragJQ", plugin_dir_url( __FILE__ ) .'../mgvo-api/libs/mitantrag/mitantragJQ.js', array( 'jquery' ), $this->version, false );
	}


	
	/**
	 * filter: [do_mgvo_shortcode] (do shortcodes in widgets)
	 */
	public function do_mgvo_shortcode($text) {
	
		return do_shortcode($text); 	

	}
	
	
	
	
	/**
	 * shortcut: [mgvo_gruppen]
	 * 
	 * Gibt eine Tabelle mit den Gruppen aus
	 * 
	 * Filter
    *    altersstufe: filtert auf das Feld hp_altersstufe in MGVO
    *    leiststufe: filtert auf das Feld hp_leiststufe in MGVO
    *    kat: filtert auf das Feld hp_kat in MGVO (Homepagekategorie)
    *    grukat: Gruppenkategorie aus MGVO
	 */
	
	public function mgvo_shortcode_gruppen($atts = [] ) {
		// outputs a list of groups
		$atts = array_change_key_case((array)$atts, CASE_LOWER);
		$a = shortcode_atts( array(
		   'altersstufe' => '',
		   'leiststufe' => '',
		   'kat' => '',
		   'abt' => '',
		   'grukat'=> '',
		   'ortid' => ''
		   
		), $atts );
		$this->func->api->api_debug(__FUNCTION__,"Übergebene Parameter:", $a);
		return $this->func->mgvo_sniplet_gruppen($a,get_page_link($this->mgvo_entrys['gruppen'])); 	

	}
		
	/**
	 * shortcut: [mgvo_gruppen_ul_li]
	 * 
	 * Gibt eine ul/li Liste mit den Gruppen aus.
	 *
	 * Filter
	 *    altersstufe: filtert auf das Feld hp_altersstufe in MGVO
	 *    leiststufe: filtert auf das Feld hp_leiststufe in MGVO
	 *    kat: filtert auf das Feld hp_kat in MGVO (Homepagekategorie)
	 *    grukat: Gruppenkategorie aus MGVO
	 */
	public function mgvo_shortcode_gruppen_ul_li($atts = []) {
	   // outputs a list of groups 
	   $atts = array_change_key_case((array)$atts, CASE_LOWER);
	   $a = shortcode_atts( array(
	      'altersstufe' => '',
	      'leiststufe' => '',
	      'kat' => '',
	      'abt' => '',
	      'grukat'=> '',
	      'ortid' => ''
	      
	   ), $atts );
	   return $this->func->mgvo_sniplet_gruppen_ul_li($a,get_page_link($this->mgvo_entrys['gruppen']));   
	}
	
	/**
	 * shortcut: [mgvo_gruppen_cards]
	 *
	 * Gibt Wochenübersicht mit Bootstap Cards aus.
	 * Einbindung von Bootstrap 4.x oder höher erfoderlich 
	 * Die Gruppeneinträge können auf Einzelseiten Verlinks werden. 
	 *
	 * Filter
	 *    altersstufe: filtert auf das Feld hp_altersstufe in MGVO
	 *    leiststufe: filtert auf das Feld hp_leiststufe in MGVO
	 *    kat: filtert auf das Feld hp_kat in MGVO (Homepagekategorie)
	 *    grukat: Gruppenkategorie aus MGVO
	 */
	public function mgvo_shortcode_gruppen_cards($atts = []) {
	   // outputs a list of groups
	   $atts = array_change_key_case((array)$atts, CASE_LOWER);
	   $a = shortcode_atts( array(
	      'altersstufe' => '',
	      'leiststufe' => '',
	      'kat' => '',
	      'grukat'=> '',
	      'abt' => '',
	      'ortid' => ''
	      
	   ), $atts );
	   return $this->func->mgvo_sniplet_group_cards($a, get_page_link($this->mgvo_entrys['gruppen']));
	}
	
	
	
	
 /*
	public function mgvo_shortcode_gruppen_bgc($atts = []) {
		// outputs a list of groups
		$atts = array_change_key_case((array)$atts, CASE_LOWER);
		$a = shortcode_atts( array(
			'category' => ''
		), $atts );	
        if ($a['category']=="") { // Beispielcode für Parameter aus der URL
			if (isset($_GET['category'])) {
				$a['category']=$_GET['category'];
			}
		} 
		return $this->func->mgvo_gen_sniplet_gruppen_bgc();

	}
*/


	/**
	 * shortcut: [mgvo_kalender]
	 */
	public function mgvo_shortcode_vkal($atts = []) {
		// outputs a list of kalender entrys
		// Es muss die Kalendernummer (aus MGVO) sowie das JAhr angegeben werden
		// mit der option "generic" <> "" wir die Generic-Funktion verwendet. Hierbei können sehr individuell alle Felder ausgegeben werden. Details in mgvo_sniplets.php 
		$atts = array_change_key_case((array)$atts, CASE_LOWER);
		$a = shortcode_atts( array(
			'nr' => '',
			'jahr' => '',
			'generic' => ''
		), $atts );	
        if ($a['nr']=="") { // Parameter aus der URL
			if (isset($_GET['vkalnr'])) {
				$a['nr']=$_GET['vkalnr'];
			} else {
				return "kein Kalender angegeben";
			}
		} 
        if ($a['jahr']=="") { //  Parameter aus der URL
			if (isset($_GET['seljahr'])) {
				$a['jahr']=$_GET['seljahr'];
			} else {
				return "kein Jahr angegeben";
			}
		} 		
        if ($a['generic'] == "") {
			$sniplet = $this->func->mgvo_sniplet_vkal($a['nr'],$a['jahr']); 	
		} else {
			$sniplet = $this->func->mgvo_gen_sniplet_vkal($a['nr'],$a['jahr']); // Hier könnten auch noch die Felder übergeben werden
		}
		return $sniplet ;
	}

		/**
	 * shortcut: [mgvo_kalender_entry]
	 */
	public function mgvo_shortcode_vkal_entry($atts = []) {
		// outputs a single kalender entry
		// Es muss die Kalendernummer (aus MGVO) sowie das Jahr angegeben werden sowie der Index angegeben werden
		// mit der option "generic" <> "" wir die Generic-Funktion verwendet. Hierbei können sehr individuell alle Felder ausgegeben werden. Details in mgvo_sniplets.php 
		$atts = array_change_key_case((array)$atts, CASE_LOWER);
		$a = shortcode_atts( array(
			'nr' => '',
			'jahr' => '',
			'index' => '',
			'generic' => ''
		), $atts );	
        if ($a['nr']=="") { // Wenn kein Parameter im Shortcode dann Parameter aus der URL holen
			if (isset($_GET['vkalnr'])) {
				$a['nr']=$_GET['vkalnr'];
			} else {
                if (isset($_GET['calnr'])) {// Kompatibilität mit fullcalender aufruf
                    $a['nr']=$_GET['calnr'];
                } else {
                    return "kein Kalender angegeben";
                }
			}
		} 
        if ($a['jahr']=="") { // Wenn kein Parameter im Shortcode dann Parameter aus der URL holen
			if (isset($_GET['seljahr'])) {
				$a['jahr']=$_GET['seljahr'];
			} else {
                if (isset($_GET['jahr'])) { // Kompatibilität mit fullcalender aufruf
                    $a['jahr']=$_GET['jahr'];
                } else {
                    return "kein Jahr angegeben";
                }
			}
		} 		
		if ($a['index']=="") { // Wenn kein Parameter im Shortcode dann Parameter aus der URL holen
			if (isset($_GET['index'])) {
				$a['index']=$_GET['index'];
			} else {
				return "kein Index angegeben";
			}
		} 
		if ($a['generic']=="") { // Wenn kein Parameter im Shortcode dann Parameter aus der URL holen
			if (isset($_GET['generic'])) {
				$a['generic']=$_GET['generic'];
			} 
		} 			
        if ($a['generic'] == "") {
			$sniplet = "Diese Funktion ist leider nicht implementiert, nutzen sie die generische Funktion (generic = 'yes')";
			// $this->func->mgvo_sniplet_vkal_entry($a['nr'],$a['jahr'],$a['index']); 	
		} else { // muss wider durch  mgvo_gen_sniplet_vkal_entry ersetzt werden
			$sniplet = $this->func->mgvo_gen_sniplet_vkal_entry2($a['nr'],$a['jahr'],$a['index']); // Hier könnten auch noch die auszugebenen Felder übergeben werden
		}
		return $sniplet ;
	}

	/**
	 * shortcut: [mgvo_gruppen_entry]
	 */
	public function mgvo_shortcode_gruppen_entry($atts = []) {
		// outputs a Group entry
		// Es muss der Index angegeben werden
		// mit der option "generic" <> "" wir die Generic-Funktion verwendet. Hierbei können sehr individuell alle Felder ausgegeben werden. Details in mgvo_sniplets.php 
		$atts = array_change_key_case((array)$atts, CASE_LOWER);
		$a = shortcode_atts( array(
			'index' => '',
			'generic' => '',
            'gruid' => ''
		), $atts );	
		//error_log("mgvo_shortcode_gruppen_entry 1 a:".print_r($a, true)." index ".$a['index']);
		if ($a['index']=="") { //  Parameter aus der URL
			if (isset($_GET['index'])) {
				$a['index'] =  $_GET['index'];
			} else {
                if ($a['gruid']==""){
                   if (isset($_GET['gruid'])) {
                        $a['gruid'] =  $_GET['gruid'];
                        //error_log("Gruid aus URL:".$a['gruid']);
                   } else { 
                        return "Weder Index noch GruppenID angegeben";
                   }
                }
                
			}
		} 
		if ($a['generic']=="") { //  Parameter aus der URL
			if (isset($_GET['generic'])) {
				$a['generic'] =  $_GET['generic'];
			} 
		}		
		error_log("mgvo_shortcode_gruppen_entry 2 a:".print_r($a, true)." index ".$a['index']);
        if ($a['generic'] == "") {
           $sniplet = $this->func->mgvo_sniplet_gruppen_entry( $a['index'], $a['gruid']);
			// $this->func->mgvo_sniplet_gruppen_entry($a['nr'],$a['jahr'],$a['index']); 	
		} else {
                      
			$sniplet = $this->func->mgvo_gen_sniplet_gruppen_entry( $a['index'], $a['gruid']); // Hier könnten auch noch die auszugebenen Felder übergeben werden
		}
		return $sniplet ;
	}
	

    /**  
	* short code: [mgvo_event_entry]
	*/
	public function mgvo_shortcode_event_entry($atts = []) {
	// outputs a Group entry
		// Es muss der Index angegeben werden
		// mit der option "generic" <> "" wir die Generic-Funktion verwendet. Hierbei können sehr individuell alle Felder ausgegeben werden. Details in mgvo_sniplets.php 
		$atts = array_change_key_case((array)$atts, CASE_LOWER);
		$a = shortcode_atts( array(
			'index' => '',
			'generic' => ''
		), $atts );	
		if ($a['index']=="") { //  Parameter aus der URL
			if (isset($_GET['index'])) {
				$a['index']=$_GET['index'];
			} else {
				return "kein Index angegeben";
			}
		} 	
        if ($a['generic'] == "") {
			$sniplet = "Diese Funktion ist leider nicht implementiert, nutzen sie die generische Funktion (generic = 'yes')";
			// $this->func->mgvo_sniplet_event_entry($a['nr'],$a['jahr'],$a['index']); 	
		} else {
			$sniplet = $this->func->mgvo_gen_sniplet_event_entry($a['index']); // Hier könnten auch noch die auszugebenen Felder übergeben werden
		}
		return $sniplet ;
	}	
		

    /**  
	* short code: [mgvo_notraining_entry]
	*/
	public function mgvo_shortcode_notraining_entry($atts = []) {
	// outputs a Group entry
		// Es muss der Index angegeben werden
		// mit der option "generic" <> "" wir die Generic-Funktion verwendet. Hierbei können sehr individuell alle Felder ausgegeben werden. Details in mgvo_sniplets.php 
		$atts = array_change_key_case((array)$atts, CASE_LOWER);
		$a = shortcode_atts( array(
			'index' => '',
			'generic' => ''
		), $atts );	
		if ($a['index']=="") { //  Parameter aus der URL
			if (isset($_GET['index'])) {
				$a['index']=$_GET['index'];
			} else {
				return "kein Index angegeben";
			}
		} 	
        if ($a['generic'] == "") {
			$sniplet = "Diese Funktion ist leider nicht implementiert, nutzen sie die generische Funktion (generic = 'yes')";
			// $this->func->mgvo_sniplet_event_entry($a['nr'],$a['jahr'],$a['index']); 	
		} else {
			$sniplet = $this->func->mgvo_gen_sniplet_notraining_entry($a['index']); // Hier könnten auch noch die auszugebenen Felder übergeben werden
		}
		return $sniplet ;
	}


	
	
	/**
	 * shortcut: [mgvo_orte]
	 */
	public function mgvo_shortcode_orte($atts = []) {
	   $a = shortcode_atts( array(
	      'link' => ''
	   ), $atts );
		// outputs a list of resources
		return $this->func->mgvo_sniplet_orte($a); 	
	}
		
	/**
	 * shortcut: [mgvo_betreuer]
	 */
	/* ersetzt durch 
	public function mgvo_shortcode_betreuer($atts = []) {
		// outputs a list of trainers
		return $this->func->mgvo_sniplet_betreuer(); 	
	}
	*/
	
	/**
	 * shortcut: [mgvo_veranstaltungen]
	 */
	public function mgvo_shortcode_veranstaltungen($atts = []) {
		// outputs a list of event
	   $a = shortcode_atts( array(
	      'kurz' => ''
	   ), $atts );
		return $this->func->mgvo_sniplet_events($a); 	
	}	
	
  	/**
	 * shortcut: [mgvo_abteilungen]
	 */
	public function mgvo_shortcode_abteilungen($atts = []) {
		// Liste der abteilungen
	   $a = shortcode_atts( array(
	      'link' => ''
	   ), $atts );
	   // outputs a list of resources 	
	   return $this->func->mgvo_sniplet_abteilungen($a); 	
	}
	
	 /**
	 * shortcut: [mgvo_trainingsausfall]
	 */
	public function mgvo_shortcode_trainingsausfall($atts = []) {

		return $this->func->mgvo_sniplet_training_fail(); 	
	}
	
	
	/**
	 * short code: [mgvo_instructors]
	 */
	public function mgvo_instructors_shortcode($atts = []) {
		// outputs a list of trainers
		return $this->func->mgvo_sniplet_betreuer(); 	
	}
		
	
	/**  
	* short code: [mgvo_instructor_by_group] 
	*/
	public function mgvo_instructor_by_group_table_shortcode($atts = []) {
	   return $this->func->mgvo_sniplet_instructor_by_group(get_page_link($this->mgvo_entrys['gruppen']));
	}
	
	
	/**
	 * short code: [mgvo_selbstauskunft]
	 */
	public function mgvo_selbstauskunft_iframe_shortcode($atts = []) {
		return $this->func->mgvo_sniplet_selbstauskunft_iframe(); 	
	}

	/**
	 * short code: [mgvo_passwort_wiederherstellen]
	 */
	public function mgvo_password_recover_link($atts = []) {
	   $atts = array_change_key_case((array)$atts, CASE_LOWER);
	   $a = shortcode_atts( array(
	      'target' => ''
	   ), $atts );
	   return $this->func->mgvo_password_recover_iframe($a['target']);
	}
	
	/**
	 * short code: [mgvo_mitgliedsantrag]
	 */
	public function mgvo_mitgliederbereich_link_shortcode($atts = []) {
	   return $this->func->mgvo_sniplet_mitgliederbereich_link(); 	
	}
	
	/**
	 * short code: [mgvo_mitgliederbereich]
	 */
	public function mgvo_mitgliedsantrag_link_shortcode($atts = []) {
	   return $this->func->mgvo_sniplet_mitantrag(); 	
	}
	
	/**
	 * short code: [mgvo_kartenbuchung]
	 */
	public function mgvo_sniplet_kartenbuchung_iframe($atts = []) {
	    $atts = array_change_key_case((array)$atts, CASE_LOWER);
		$a = shortcode_atts( array(
			'eventnr' => '',
			'target' => 'iframe'
		), $atts );	
		if ($a['eventnr']=="") { //  Parameter aus der URL
			if (isset($_GET['eventnr'])) {
				$a['eventnr']=$_GET['eventnr'];
			} else {
				return "keine Eventnr angegeben";
			}
		} 	
		return $this->func->mgvo_sniplet_kartenbuchung_iframe($a['eventnr'],$a['target']); 	
	}
	
	/**
	 * short code: [mgvo_document_link]
	 */
	function mgvo_document_link_shortcode ($atts = []) {
		$atts = array_change_key_case((array)$atts, CASE_LOWER);
		$a = shortcode_atts( array(
			'dokbez' => ''
		), $atts );	
		if ($a['dokbez']=="") { //  Parameter aus der URL
			if (isset($_GET['dokart'])) {
				$a['dokbez']=$_GET['dokbez'];
			} else {
				return "keine Dokumentname angegeben, bitte dokbez='xxxx' benutzen";
			}
		} 	
		return $this->func->mgvo_sniplet_document_link($a['dokbez']);
	}

	/**
	 * short code: [mgvo_mitgliederliste]
	 */
	function mgvo_mitgliederliste_shortcode ($atts = []) {
		 // Allgemeiner Suchbegriff: suchbeg
         // Suchalter/Geburtsdatum: suchalterv - suchalterb
         // Austritt: suchaustrittv - suchaustrittb
         // Gruppen-ID: suchgruid
         // Beitragsgruppe: suchbeigru
         // Lastschriftzahler: lssel (Selektionswert: 1)
         // Barzahler/Überweiser: barsel (Selektionswert: 1)
         // Dauerauftrag: dasel (Selektionswert: 1)
         // Geschlecht: geschl (x,m,w)
         // Mitglied: ausgetr (x,m,a)
         // Aktiv/Passiv: aktpass (x,a,p)
         // Mailempfänger: mailempf (x,e,s)
		$atts = array_change_key_case((array)$atts, CASE_LOWER);
		$a = shortcode_atts( array(
			'suchbeg' => '',
			'suchalterv' => '',
			'suchalterb' => '',
			'suchaustrittv' => '',
			'suchaustrittb' => '',
			'suchgruid' => '',
			'lssel' => '',
			'barsel' => '',
			'dasel' => '',
			'geschl' => '',
			'ausgetr' => '',
			'aktpass' => '',
			'mailempf' => ''
		), $atts );	

		return $this->func->mgvo_sniplet_read_mitglieder($a);
	}
	/**
	 * short code: [mgvo_mitglied]
	 */
	function mgvo_mitglied_shortcode ($atts = []) {
		$atts = array_change_key_case((array)$atts, CASE_LOWER);
		$a = shortcode_atts( array(
			'mgnr' => ''
		), $atts );	
		if ($a['mgnr']=="") { //  Parameter aus der URL
			if (isset($_GET['mgnr'])) {
				$a['mgnr']=$_GET['mgnr'];
			} else {
				return "keine Mitgliedsnummer angegeben, bitte mgnr='xxxx' benutzen";
			}
		} 	
		return $this->func->mgvo_sniplet_show_mitglied($a['mgnr']);
	}
	/**
	 * short code: []
	 */
	function mgvo_mitglied_picture_shortcode ($atts = []) {
		$atts = array_change_key_case((array)$atts, CASE_LOWER);
		$a = shortcode_atts( array(
			'mgnr' => ''
		), $atts );	
		if ($a['mgnr']=="") { //  Parameter aus der URL
			if (isset($_GET['mgnr'])) {
				$a['mgnr']=$_GET['mgnr'];
			} else {
				return "keine Mitgliedsnummer angegeben, bitte mgnr='xxxx' benutzen";
			}
		} 	
		return $this->func->mgvo_sniplet_mitpict($a['mgnr']);
	}
	/**
	 * short code: []
	 */
	function mgvo_authentication ( $username) {
        global $wpdb;
		error_log("mgvo_authentication called");
		// Don't authenticate twice
		if (empty($username)) {
			error_log("MGVO: mgvo_authentication:  kein User übergeben");
			return NULL;
        }

		//$username=$_POST['log'];
		$password=$_POST['pwd'];

		//error_log("mgvo_authentication User ".$username." pw ".$_POST['pwd']); 

        //loginset = new MGVO_HPAP($this->call_id = $options['mgvo_call_id']);
		$mgvo_result = $this->func->api->login($username,$password,"","");
		
		error_log("mgvo_authentication User Result ".$mgvo_result);
        //if ($this->func->api->login($username,$password,"","") == 1) {
		if ($username == "test") {
			$userinfo = get_user_by( 'login', 'defaultuser_mgvo' );
			//$property = $wpdb->prefix . 'capabilities';
			//$caps = $userinfo->$property;
			error_log("mgvo_authentication user: ".$username." => OK".print_r($userinfo,true));
			$user = new WP_User;
			$user->init( $userinfo );
		    error_log("mgvo_authentication user obj: ".print_r($user,true));      
			wp_set_current_user($user->ID, $user->user_login);
			wp_set_auth_cookie($user->ID);
			do_action('wp_login', $user->user_login);
			//header("Location:".get_page_link(MY_PROFILE_PAGE));
			wp_redirect( home_url() );
			return $user; // $user->ID
		} else {
	 		    error_log("mgvo_authentication user: ".$username." => Fail");
				wp_clear_auth_cookie();
				return NULL;
				
		}
    }
    /**
     * Lenkt das Passwort-Recovery von Wordpress um
     * Achtung: Per Default ist dies ausgeschaltet / Auskommentiert. 
     */
	function mgvo_redirect_home( $lostpassword_redirect ) {
		$url = "https://www.mgvo.de/prog/pub_pwrequest.php?call_id=".$this->call_id;
		error_log("mgvo_redirect_home :".$url);
		// Zum Einschalten diese Zeile Kommentarzeichen entfernen
		//wp_redirect($url); // darüber dann die WP-Passwortrecover-Seite umgelenkt werden
		return ;
	}

	/**
	 * short code: [mgvo_mitantrag]
	 */

	function mgvo_shortcode_mitantrag($atts = [])  {
	   $atts = array_change_key_case((array)$atts, CASE_LOWER);
	   $a = shortcode_atts( array(
	      'form' => '',    
	   ), $atts );
	   $form = $a['form'] == '' ? $this->mitantragForm : $a['form'];
	   $html = $this->func->mgvo_sniplet_mitantrag($this->mitantragSettings, false, $form);
	   return $html;
	}
	
	function mgvo_shortcode_monitor($atts = [])  {
	   
	   $all_pages = get_pages();
	   
	   foreach( $all_pages as $page ) {	
	        // suchen nach Seiten mit "monitor" im Namen.
	      // if ($page->name );
	      
	      echo esc_html( $page->post_name ); // Nur zum Testen/Entwicklen!
            
            // besser wäre nach allen Seiten zu suchen, die den Shortcode enthalten. Das ist aber schwiergier
            
            // if ($post_content ) ...
            
	      // Oder die DB-Lösung von https://stackoverflow.com/questions/53875931/list-all-pages-containing-shortcode-wordpress
            /*
             *  global $wpdb;
                $query = $wpdb->prepare(
                     "SELECT id, post_title, post_status
                             FROM  $wpdb->posts
                             WHERE (post_content LIKE %s OR post_content LIKE %s)
                             AND post_status IN ('publish', 'private', 'draft') 
                             ORDER BY post_title",
                    '%[' . $wpdb->esc_like($tag) . ' %', // i.e. [tag param..]
                    '%[' . $wpdb->esc_like($tag) . ']%' // i.e. [tag]
                );
             */
	   }
	   
	   // Das Javascript hier unten muss nun nur noch dynamisch an die oben erzeugte Liste angepasst werden. 
	   // Außerdem sollte das Script per curl o.ä. vorher prüfen ob die Seite wirklich abrufbar ist. Sonst wird die nächste Seite verwendet. 
	   // Ist keine andere Seite verfügbar, dann retry in 10min
	   // (Hintergrund: Wenn der Webserver mal kurz weg ist, hängt sonst der Monitor den ganzen Tag, da nur noch eine HTTP 404 o.ä. angezeigt wird. 
	   

	     
	   
	   
	   return $html;
	}
	

}
