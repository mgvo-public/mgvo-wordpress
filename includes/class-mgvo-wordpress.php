<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @since      1.0.0
 *
 * @package    Mgvo_Wordpress
 * @subpackage Mgvo_Wordpress/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Mgvo_Wordpress
 * @subpackage Mgvo_Wordpress/includes
 * @author     Michael Kours
 */
class Mgvo_Wordpress {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Mgvo_Wordpress_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

		/**
	 * Sanitizer for cleaning user input
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      MGVO_Sanitize    $sanitizer    Sanitizes data
	 */
	private $sanitizer;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    0.1.0
	 */
	public function __construct() {
		if ( defined( 'PLUGIN_MGVO_WORDPRESS_VERSION' ) ) {
			$this->version = PLUGIN_MGVO_WORDPRESS_VERSION;
		} else {
			$this->version = '0.1.0';
		}
		$this->plugin_name = 'mgvo-wordpress';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Mgvo_Wordpress_Loader. Orchestrates the hooks of the plugin.
	 * - Mgvo_Wordpress_i18n. Defines internationalization functionality.
	 * - Mgvo_Wordpress_Admin. Defines all hooks for the admin area.
	 * - Mgvo_Wordpress_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-mgvo-wordpress-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-mgvo-wordpress-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-mgvo-wordpress-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-mgvo-wordpress-public.php';

				/**
		 * The class responsible for sanitizing user input
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-mgvo-wordpress-sanitize.php';

		$this->sanitizer = new MGVO_Wordpress_Sanitize();

		$this->loader = new Mgvo_Wordpress_Loader();



	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Mgvo_Wordpress_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Mgvo_Wordpress_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Mgvo_Wordpress_Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
		$this->loader->add_action( 'admin_menu', $plugin_admin, 'add_menu' );
		$this->loader->add_action( 'admin_init', $plugin_admin, 'register_settings' );		
		$this->loader->add_action( 'admin_init', $plugin_admin, 'register_sections' );
		$this->loader->add_action( 'admin_init', $plugin_admin, 'register_fields' );

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Mgvo_Wordpress_Public( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );
		$this->loader->add_shortcode('mgvo_gruppen', $plugin_public, 'mgvo_shortcode_gruppen');
		$this->loader->add_shortcode('mgvo_kalender', $plugin_public, 'mgvo_shortcode_vkal');
		$this->loader->add_shortcode('mgvo_orte', $plugin_public, 'mgvo_shortcode_orte');
		$this->loader->add_shortcode('mgvo_veranstaltungen', $plugin_public, 'mgvo_shortcode_veranstaltungen');
		$this->loader->add_shortcode('mgvo_abteilungen', $plugin_public, 'mgvo_shortcode_abteilungen');
		$this->loader->add_shortcode('mgvo_trainingsausfall', $plugin_public, 'mgvo_shortcode_trainingsausfall');
		$this->loader->add_shortcode('mgvo_kalender_entry', $plugin_public, 'mgvo_shortcode_vkal_entry');
		$this->loader->add_shortcode('mgvo_gruppen_entry', $plugin_public, 'mgvo_shortcode_gruppen_entry');
		$this->loader->add_shortcode('mgvo_event_entry', $plugin_public, 'mgvo_shortcode_event_entry');
		$this->loader->add_shortcode('mgvo_notraining_entry', $plugin_public, 'mgvo_shortcode_notraining_entry');
		$this->loader->add_shortcode('mgvo_instructor_by_group', $plugin_public, 'mgvo_instructor_by_group_table_shortcode');
		$this->loader->add_shortcode('mgvo_instructors', $plugin_public, 'mgvo_instructors_shortcode');
		$this->loader->add_shortcode('mgvo_selbstauskunft', $plugin_public, 'mgvo_selbstauskunft_shortcode');
		$this->loader->add_shortcode('mgvo_mitgliedsantrag', $plugin_public, 'mgvo_mitgliedsantrag_link_shortcode');
		$this->loader->add_shortcode('mgvo_mitgliederbereich', $plugin_public, 'mgvo_mitgliederbereich_link_shortcode');
		$this->loader->add_shortcode('mgvo_passwort_wiederherstellen', $plugin_public, 'mgvo_password_recover_link');
		$this->loader->add_shortcode('mgvo_document_link', $plugin_public, 'mgvo_document_link_shortcode');
		$this->loader->add_shortcode('mgvo_mitgliederliste', $plugin_public, 'mgvo_mitgliederliste_shortcode');
		$this->loader->add_shortcode('mgvo_mitglied', $plugin_public, 'mgvo_mitglied_shortcode');
		$this->loader->add_shortcode('mgvo_mitglied_picture', $plugin_public, 'mgvo_mitglied_picture_shortcode');
		$this->loader->add_shortcode('mgvo_list_documents', $plugin_public, 'mgvo_list_documents_shortcode');
		$this->loader->add_shortcode('mgvo_kartenbuchung', $plugin_public, 'mgvo_sniplet_kartenbuchung_iframe');
      $this->loader->add_shortcode('mgvo_mitantrag', $plugin_public, 'mgvo_shortcode_mitantrag');
      $this->loader->add_shortcode('mgvo_gruppen_ul_li', $plugin_public, 'mgvo_shortcode_gruppen_ul_li');
      $this->loader->add_shortcode('mgvo_gruppen_cards', $plugin_public, 'mgvo_shortcode_gruppen_cards');
      $this->loader->add_shortcode('mgvo_monitor', $plugin_public, 'mgvo_shortcode_monitor');
      
      //wp_register_style( 'mgvo_bt_style','https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css' );
      //wp_register_script( 'mgvo_bt','https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js' );
      
        
        

		$this->loader->add_filter('widget_text', $plugin_public, 'do_mgvo_shortcode',13);
		
		//$this->loader->add_action( 'wp_authenticate', $plugin_public, 'mgvo_authentication' );
		$this->loader->add_filter( 'lostpassword_redirect', $plugin_public, 'mgvo_redirect_home' );
		
		function add_cors_http_header(){
		   //header("Access-Control-Allow-Origin: *");
		}
		add_action('init','add_cors_http_header');



		add_action( 'wp_enqueue_scripts', 'mgvo_bt_enqueue_scripts');

 	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Mgvo_Wordpress_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}
}

function mgvo_bt_enqueue_scripts() {
	global $post;
	wp_register_style( 'mgvo_bt_style','https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css' );
	wp_register_script( 'mgvo_bt','https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js' );
	
	if( is_a( $post, 'WP_Post' ) && has_shortcode( $post->post_content, 'mgvo_mitantrag') ) {
	   $options = get_option( "mgvo-wordpress" . '-options' );
	   if (true or (isset ($options['mgvo_mitantrag_form']) &&  $options['mgvo_mitantrag_form'] != "")) {
   //		wp_register_style( 'mgvo_bt_style','https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css' );
   		wp_enqueue_style( 'mgvo_bt_style');
   //		wp_register_script( 'mgvo_bt','https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js' );
   		wp_enqueue_script( 'mgvo_bt' );
	   }
	}
}
