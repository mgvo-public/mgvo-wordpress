<?php

/**
 * Fired during plugin activation
 *
 * @since      1.0.0
 *
 * @package    Mgvo_Wordpress
 * @subpackage Mgvo_Wordpress/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Mgvo_Wordpress
 * @subpackage Mgvo_Wordpress/includes
 * @author     Michael Kours
 */
class Mgvo_Wordpress_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

		install_events_pg("Kalenderdetails","[mgvo_kalender_entry]");
		install_events_pg("Gruppendetails","[mgvo_gruppen_entry]");
		install_events_pg("Veranstaltungsdetails","[mgvo_event_entry]");
		install_events_pg("Trainingsausfall Details","[mgvo_notraining_entry]");
		
	}
	

}
//Add Events page on activation:
	 function install_events_pg($title, $content){
		$new_page_title = $title;
		$new_page_content = $content;
		$new_page_template = ''; //ex. template-custom.php. Leave blank if you don’t want a custom page template.
		//don’t change the code below, unless you know what you’re doing
		$page_check = get_page_by_title($new_page_title);
		$new_page = array(
				'post_type' => 'page',
				'post_title' => $new_page_title,
				'post_content' => $new_page_content,
				'post_status' => 'publish',
				'post_author' => 1,
		);
		if(!isset($page_check->ID)){
				$new_page_id = wp_insert_post($new_page);
				if(!empty($new_page_template)){
						update_post_meta($new_page_id, '_wp_page_template', $new_page_template);
				}
		}
	}

